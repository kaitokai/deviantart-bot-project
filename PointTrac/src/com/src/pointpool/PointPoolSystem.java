package com.src.pointpool;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import com.src.source.Common;
import com.src.source.Entity;

public class PointPoolSystem {
//	private static final String PURCHASE_LOG = "purchaselog.txt";
	private static final String TCA_FUNDS = "tca_fund.txt";
//	private static ArrayList<String> pLogs = new ArrayList<>();
	
	private static final double TAX_RATE = 0.10;
	private static int TCAGeneralFund = 0;
	
	public static int getGenFunds(){
		return TCAGeneralFund;
	}// end routine getGenFunds
	
	/**
	 * 
	 * @param by
	 * @param slr
	 * @param iName
	 * @param count
	 * @param cost
	 * @return
	 */
	public static boolean buy(Entity byr, Entity slr, String iName, int count) {
		if(Common.DEBUGGING){
			System.out.println("Entered routine Buy: " + byr.getName() + " " + 
					slr.getName() + " " + iName + " " + count);
		}
		
		if(!Item.hasItem(iName)) return false; // does this item exist in db?
		
		int itemCost = Item.getItem(iName).getCost();
		double salePcnt = (slr instanceof Store)?((Store)slr).getSalePrcnt():1;
		// Calculate the sale cost of the store
		int totalCost = (int)Math.ceil(itemCost * salePcnt) * count;
		if(Common.DEBUGGING)
			System.out.println("\tTotal Cost: " + totalCost);
		// ENSURE that the item is there and the buyer has the points required
		if (byr.getPoints() < totalCost){
			if(Common.DEBUGGING) 
				System.out.println("\tBuyer has insufficent points");
			return false;
		}

		// Exchange points, subtract the tax
		int tax = (int)((slr.getName().equalsIgnoreCase("TCA"))?totalCost:(TAX_RATE * totalCost));
		if(Common.DEBUGGING)
			System.out.println("\tTax: " + tax + " points");
		if (slr.tradeItem(iName, count, byr)) {
			if(Common.DEBUGGING) System.out.println("\tTrade: Successful");
			String reciept = "-" + totalCost + " pts, " + 
					byr.getName() + " purchase from " + 
					slr.getName() + " [" + iName + " x " + count +"]";
			if(byr.givePointsTo(totalCost, slr, reciept)){
				if(Common.DEBUGGING){
					System.out.println("\tPoints exchanged");
					System.out.println("Exiting routine\n");
				}
				
				TCAGeneralFund += tax;
				Common.saveData(TCA_FUNDS, TCAGeneralFund);
				return slr.givePointsTo(tax, null, " tax to general funds");
			}
			if(Common.DEBUGGING) System.out.println("\tCould not give " + totalCost);
		}
		if(Common.DEBUGGING) System.out.println("\tItem: " + iName + " could not be traded");
		return false;
	}// end routine Buy
	/**
	 * Sells routine to other entity
	 */
	public static void sell(){
		// TODO: 
	}// end routine sell
	
	/**
	 * Subsidy system: 
	 * RATES:
	 * 		05%: 050 points per hour
	 * 		10%: 100 points per hour
	 * 		50%: 500 points per hour
	 * 		75%: 750 points per hour
	 * 
	 * Requirements:
	 * - Minimum of five (5) hours must be purchased with the General Fund
	 * - 
	 * - 
	 * 
	 * @return <tt>true</tt> if subsidy is approved by the shop
	 */
	public static boolean enstateSubsidy(Store store, int percentage, int time){
		
		return false;
	}
	
	/**
	 * 
	 * @param giver
	 * @param receiver
	 * @param iName
	 * @return
	 */
	public static boolean trade(Entity giver, Entity receiver, int quantity, String iName) {
		return giver.tradeItem(iName, quantity, receiver);
	}// end routine Trade

	/**
	 * PRECONDITION:<br/>
	 * POSTCONDITION: If successful steal, item added to user inventory
	 * and item removed from shop stock. Failure rate raises by 5% unless
	 * at cap: 88% failure rate.<br/>
	 * 
	 * @param theif
	 * @param store
	 * @param iName
	 * @return <tt>true</tt>, iff the steal attempt is successful<br/>
	 * 		   <tt>false</tt>, if otherwise
	 */
	public static boolean steal(Entity theif, Entity store, String iName){
		// TODO:
		return false;
	}// end routine Steal
	
	/**
	 * Loads all PointPool related items
	 */
	public static void load(){
		Common.readData(Store.STORE_DATA, Store.storeList);
		TCAGeneralFund = Common.readData(TCA_FUNDS);
		new Thread(new StoreTimer()).start();
	}// end routine load
	
	public static String getRestockSchedule(){
		return "Shops always restock at midnight P.S.T.";
	}
}// end class PointPoolSystem

class StoreTimer implements Runnable{
	/**
	 * This routine calls the update on each shop in the arraylist
	 */
	
	@Override
	public void run() {
		while(true){
			try{
				TimeUnit.SECONDS.sleep(1);
				
				Calendar cal = Calendar.getInstance();
				int hour = cal.get(Calendar.HOUR_OF_DAY);
				int minute = cal.get(Calendar.MINUTE);
				int second = cal.get(Calendar.SECOND);
				if(hour == 0 && minute == 0 && second == 0){
					System.out.println("Restocking shops...");
					for(Store elm : Store.storeList){
						elm.restock();
					}
					System.out.println("All shops restocked.");
					Common.saveData(Store.STORE_DATA, Store.storeList);
					Common.backup();
				}
			}catch(InterruptedException ex){
				ex.printStackTrace();
				break;
			}
		}
	}
}// end inner class StoreTimer