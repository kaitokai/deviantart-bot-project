package com.src.pointpool;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.HashMap;

import com.src.pointtrack.DAFmts;
import com.src.pointtrack.FCharacter;
import com.src.pointtrack.PointTrack;
import com.src.source.Common;
import com.src.source.Entity;

public class Store extends BusinessEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6557416186612278934L;
	public static ArrayList<Store> storeList = new ArrayList<>();
	public static final String STORE_DATA = "shop.dat";

	// FIELDS
	private Inventory uStock;
	private ArrayList<ItemPair> properway = new ArrayList<>();
	
	public final int updateIntervals = 10;
	private String description;
	private String operator;
	
	// CONSTRUCTORS
	private Store(String name, String owner, String operator, String description, 
			int worth, Inventory uInventory) {
		super(name, owner, worth);
		this.uStock = (uInventory == null)?new Inventory():uInventory;

		this.description = description;
		this.operator = operator;
		Common.saveData(STORE_DATA, storeList);
	}// end
	private Store(String name, String owner, String operator, String description, int worth){
		this(name, owner, operator, description, worth, null);
	}
	
	// INSTANTIATORS
	/**
	 * 
	 * @param name
	 * @param owner
	 * @param worth
	 * @return A <tt>Store</tt> object if a store does not exist with the same
	 * name. Otherwise, returns <tt>null</tt>.
	 */
	public static Store createInstance(String name, String owner, String operator, String description, int worth){
		Store store = getStore(name);
		if(store != null) return null;
		store = new Store(name, owner, operator, description, worth);
		storeList.add(store);
		
		Common.saveData(STORE_DATA, storeList);
		return store;
	}// end routine createInstance
	
	/**
	 * <p>Limited edition things are added here, they are non-replenished items
	 * that will probably be given away during events or limited sale.
	 * 
	 * <p>POSTCONDITION: Item will be added to the personal inventory. If the 
	 * item is in the personal inventory, the quantity will be added.
	 * 
	 * @param itemName
	 * @param quantity
	 * @return <tt>true</tt> if the item exists and the quantity is greater 
	 * than zero
	 */
	public boolean addSpecialItem(String itemName, int quantity){
		Item item = Item.getItem(itemName);
		if(item == null || quantity <= 0) return false;
		
		if(!inventory.addItem(itemName, quantity)){
			inventory.getPair(item).modifyCount(quantity);
		}
		
		Common.saveData(STORE_DATA, storeList);
		return true;		
	}// end routine addSpecialItem
	
	/**
	 * <p>Regularly updated items are added to the store's regular stock.
	 * 
	 * PRECONDITION:<br>
	 * POSTCONDITION: Item is added to the shop list, so long as it doesn't
	 * exist within the list itself.<br>
	 * @param itemName
	 * @param quantity
	 * @return
	 */
	public boolean addRegularStock(String itemName, int quantity){
		Item item = Item.getItem(itemName); // insure the item exists
		if(item == null || quantity <= 0) return false;
		
		if(ItemPair.getPair(item, properway) != null){
			// this item already exists in our stock already
			return false;
		}
		else{
			// we add the item to the number keeper
			properway.add(new ItemPair(item, quantity));
		}
		
		boolean isGood = uStock.addItem(itemName, quantity);
		Common.saveData(STORE_DATA, storeList);
		return isGood;
	}// end routine addRegularStock
	
	public boolean removeFromStock(Item item){
		if(uStock.removeFromInventory(item.getName())){
			properway.remove(ItemPair.getPair(item, properway));
			
			Common.saveData(STORE_DATA, storeList);
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @param itemName
	 * @param quantity
	 * @return
	 */
	public boolean modifyStockAmount(String itemName, int quantity){
		// grab the index to see if the item exists within the
		Item item = Item.getItem(itemName);
		if(item == null || quantity <= 0) return false;
		
		uStock.getPair(item).modifyCount(quantity);
		return true;
	}
		
	/**
	 * 
	 * @param itemName
	 * @return
	 */
	public Inventory properInventory(String itemName){
		if(inventory.getItemCount(itemName) != -1)
			return inventory;
		else if(uStock.getItemCount(itemName) != -1)
			return uStock;
		return null;
	}// end routine properInventory
	
	public void restock(){
		if(properway == null){
			System.out.println(name);
		}
		else
		for(int i = 0; i < properway.size(); i++){
			ItemPair pair = properway.get(i);
			ItemPair stock = uStock.getPair(pair.getItem());
			
			if(stock != null){
				uStock.getPair(pair.getItem()).setQuantity(pair.getQuantity());
			}
			else{
				uStock.addItem(pair.getItem().getName(), pair.getQuantity());
			}
		}
	}// end routine restock
	
	@Override
	public String printInventory() {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		sb.append("<b>Personal Stock:</b>\n");
		sb.append("---------------------------\n");
		sb.append(inventory.toInvString(salePrcnt) + "\n");
		sb.append("<b>Store Items:</b>\n");
		sb.append("---------------------------\n");
		sb.append(uStock.toInvString(salePrcnt));
		return sb.toString();
	}// end routine
	
	private double salePrcnt = 1; // used for discounts
	/**
	 * 
	 * @param prcnt must be greater than or equal to 0
	 */
	public void setSalePrcnt(double prcnt){
		salePrcnt = prcnt;
	}//
	public double getSalePrcnt(){
		return salePrcnt;
	}//
		
	public void setOperator(String operator) {
		this.operator = operator;
		Common.saveData(STORE_DATA, storeList);
	}
	public String getOperator(){
		return operator;
	}

	/**
	 * 
	 * @param name the name of the store
	 * @return <tt>Store</tt> object if it is within the list, <tt>null</tt>
	 * otherwise.
	 */
	public static Store getStore(String name){
		for(Store e : storeList){
			if(e.name.equalsIgnoreCase(name)){
				return e;
			}
		}
		return null;
	}// end routine getStore
	
	public static boolean hasStore(String name){
		return (getStore(name) == null)? false : true;
	}// end routine hasStore
	
	public static Store removeStore(String name){
		Store store = getStore(name);
		storeList.remove(store);
		
		Common.saveData(STORE_DATA, storeList);
		return store;
	}
	
	public static String listStores(){
		StringBuilder sb = new StringBuilder();
		sb.append("<b><code>" +
				DAFmts.daSpcFmtr("SHOP NAME", 20, DAFmts.LEFT) +
				DAFmts.daSpcFmtr("OPERATOR", 20, DAFmts.LEFT) + 
				"DESCRIPTION" +
				"</code></b>\n");
		for(Store elem : storeList){
			sb.append("<b><code>" +
					DAFmts.daSpcFmtr(elem.name, 20, DAFmts.LEFT) + 
					"</code></b><code>" +
					DAFmts.daSpcFmtr(elem.operator, 20, DAFmts.LEFT) + 
					elem.description + "</code>\n");
		}
		return sb.toString();
	}
	
	// BLACKLIST & LIMITATIONS CODE:
	private ArrayList<FCharacter> blackList = new ArrayList<>();
	public boolean addToBlacklist(String characterName){
		// TODO 
		return false;
	}// end routine	
	public FCharacter removeFromBlacklist(){
		// TODO
		return null;
	}// end routine
	
//	private ArrayList<Level> limitlist;
	
	// ENTITY IMPLEMENTATION:
	@Override
	public boolean givePointsTo(int points, Entity entity, String reason) {
		if(worth < points) return false;
		worth -= points;
		if(entity == null) return true;
		return entity.addPoints(this, points, reason);
	}

	@Override
	public boolean tradeItem(String itemName, int quantity, Entity entity) {
		Inventory inv = entity.getInventory(); // simplify length of code
		
		if(inventory.getItemCount(itemName) != -1){
			if(inventory.exchangeItem(inv, itemName, quantity)){
				Common.saveData(STORE_DATA, storeList);
				return true;
			}
		}
		if(uStock.getItemCount(itemName) != -1){
			if(uStock.exchangeItem(inv, itemName, quantity)){
				Common.saveData(STORE_DATA, storeList);
				return true;
			}
		}
		
		return false;
	}// end routine

	@Override
	public int getPoints() {
		return getWorth();
	}// end routine

	@Override
	public boolean addPoints(Entity entity, int points, String reason) {
		if(points < 0) return false;
		this.worth += points;
		this.totalEarnings += points;

		Common.saveData(STORE_DATA, storeList);
		return true;
	}// end routine
	
	@Override
	public boolean equals(Object obj){
		if(this == obj) return true;
		if(!(obj instanceof Store)) return false;
		Store store = (Store) obj;
		if(this.name.equalsIgnoreCase(store.name)) return true;
		return false;
	}// end routine
	
	public static void save(){
		Common.saveData(STORE_DATA, storeList);
	}
}// end class Store
