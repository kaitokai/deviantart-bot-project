package com.src.pointpool;

import com.src.source.Entity;
@SuppressWarnings ("serial")
public abstract class BusinessEntity extends Entity{
	protected String owner;
	
	protected int worth;
	protected int totalEarnings;
	
	public BusinessEntity(String name, String owner, int worth, Inventory stock){
		this.name = name;
		this.owner = owner;
		this.worth = worth;
		this.inventory = (stock == null)?new Inventory():stock;
		totalEarnings = 0;
	}
	public BusinessEntity(String name, String owner, int worth){
		this(name, owner, worth, null);
	}
	public BusinessEntity(String name, String owner){
		this(name, owner, 0, null);
	}
	public BusinessEntity(String name){
		this(name, "John Doe", 0, null);
	}
	
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public int getWorth() {
		return worth;
	}
	public void setWorth(int worth) {
		this.worth = worth;
	}
	public void modifyWorth(int points){
		this.worth += points;
	}
	public int getTotalEarnings() {
		return totalEarnings;
	}
	public void setTotalEarnings(int totalEarnings) {
		this.totalEarnings = totalEarnings;
	}
	
	public abstract String printInventory();
}// end
