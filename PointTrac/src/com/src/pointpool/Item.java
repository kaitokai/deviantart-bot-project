package com.src.pointpool;
import java.util.ArrayList;
import java.util.Collections;
import java.io.Serializable;

import com.src.pointtrack.*;
import com.src.source.Common;
public class Item implements Serializable, Comparable<Item>{
	private static final long serialVersionUID = 4862722450229644491L;
	private static final String ITEM_FILE = "items.dat";
	public transient static ArrayList<Item> masterList = new ArrayList<>();
	
	// FIELDS
	private String name;
	private String desc;
	//private Brand brand;
	private Catagory catagory;
	private int cost;
	private boolean isUsable = false;
	private String useMsg = "This item isn't usable at the moment";
	
	private Item [] prizes = null;
	
	// Dummy constructor, do not use for application
	private Item(String name){
		this.name = name;
	}
	private Item(String name, String desc, int cost, boolean isUsable, 
			String useMsg, Catagory catagory){
		this.name = name;
		this.desc = desc;
		this.cost = cost;
		this.isUsable = isUsable;
		this.useMsg = useMsg;
		this.catagory = catagory;
	}// end
	private Item(String name, String desc, int cost, boolean isUsable,
			Catagory catagory, Item... prizes){
		this(name, desc, cost, isUsable, "none", catagory);
		java.util.Random rand = new java.util.Random();

		int locat, positions = 0;
		Item[] mixed = new Item[prizes.length];
		while(positions < prizes.length){
			locat = rand.nextInt(prizes.length);
			if(mixed[locat] == null){
				mixed[locat] = prizes[positions++];
			}
		}		
		this.prizes = mixed;
	}
	
	// Routines
	@Override
	public String toString(){
		return "[Name=" + name + ", " +
				"Description=" + desc + ", " +
				"Cost=" + cost + ", " +
				"Usable Item? " + ((isUsable)?"yes":"no") + ", " +
				"Usage Message=" + useMsg + "]";
	}// end routine toString

	@Override
	public int compareTo(Item o) {
		return this.name.toLowerCase().compareTo(o.name.toLowerCase());
	}// end 
	
	@Override
	public boolean equals(Object o){
		if(this == o) return true;
		if(!(o instanceof Item)) return false;
		Item cmp = (Item)o;
		if(this.name.equalsIgnoreCase(cmp.name)){
			return true;
		}
		return false;
	}// end
		
	/**
	 * 
	 * @param name
	 * @param desc
	 * @param cost
	 * @param isUsable
	 * @param useMsg
	 * @param catagory
	 * @return
	 */
	public static boolean createItem(String name, String desc, int cost, 
			boolean isUsable, String useMsg, Catagory catagory){
		if(hasItem(name)) return false;
		
		masterList.add(new Item(name, desc, cost, isUsable, useMsg, catagory));
		Collections.sort(masterList);
		
		Common.saveData(ITEM_FILE, masterList);
		return true;
	}// end routine createItem
	public static boolean createItem(String name, String desc, int cost, Item... prizes){
		if(hasItem(name)) return false;
		
		masterList.add(new Item(name, desc, cost, true, Catagory.CHEST, prizes));
		Collections.sort(masterList);
		
		Common.saveData(ITEM_FILE, masterList);
		return true;
	}
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	public static Item getItem(String name){
		int pos = Collections.binarySearch(masterList, new Item(name));
		if(pos < 0) return null;
		return masterList.get(pos);
	}// end routine getItem
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	public static boolean hasItem(String name){
		return (getItem(name) == null)? false : true;
	}// end routine hasItem
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	public static boolean deleteItem(String name){
		Item itm = getItem(name);
		if(itm == null) return false;
		
		for(Store elem : Store.storeList){
			elem.getInventory().removeFromInventory(itm.name);
			elem.removeFromStock(itm);
		}
		for(FCharacter elem : CharManager.cList){
			elem.getInventory().removeFromInventory(itm.name);
		}
		Common.saveData(ITEM_FILE, masterList);

		return masterList.remove(itm);
	}// end routine deleteItem
	
	// CONSTANTS
	private static final int ITM_SPCR = 45;
	private static final int BC_SPCR = 15;
	/**
	 * 
	 * @return
	 */
	public String daString(){
		String lst = "<code>" + 
				DAFmts.daSpcFmtr(name, ITM_SPCR, DAFmts.LEFT) +
				DAFmts.daSpcFmtr(String.valueOf(cost), BC_SPCR, DAFmts.LEFT) +
				catagory.toString() +
				"</code>\n";
		return lst;
	//		return name + " Base Cost:" + cost + " " + catagory.toString() + "\n";
	}// end routine

	/**
	 * 
	 * @return
	 */
	public static String itemList(){
		StringBuilder sb = new StringBuilder();
		sb.append("<b><code>" + 
				DAFmts.daSpcFmtr("ITEM NAME", ITM_SPCR, DAFmts.LEFT) +
				DAFmts.daSpcFmtr("BASE COST", BC_SPCR, DAFmts.LEFT) +
				"CATEGORY" +
				"</code></b>\n");
		for(Item elem : masterList){
			sb.append(elem.daString());
		}
		return sb.toString();
	}// end routine itemList

	/**
	 * 
	 * @param c
	 * @return
	 */
	public static String itemListByCatagory(Catagory c){
		StringBuilder sb = new StringBuilder();
		sb.append("<b><code>" + 
				DAFmts.daSpcFmtr("ITEM NAME", ITM_SPCR, DAFmts.LEFT) +
				DAFmts.daSpcFmtr("BASE COST", BC_SPCR, DAFmts.LEFT) +
				"CATEGORY" +
				"</code></b>\n");
		for(Item elem : masterList){
			if(elem.catagory.equals(c)){
				sb.append(elem.daString());
			}
		}
		return sb.toString();
	}
	
	public static int itemCount(){
		return masterList.size();
	}
	
	// Helper
	
	// Accessors and Mutators
	public String getName() {
		return name;
	}
	public void setName(String name) {
		for(Store elem : Store.storeList){
			Inventory inv = elem.properInventory(this.name);
			if(inv != null) inv.getPair(this).getItem().name = name;			
		}
		for(FCharacter elem : CharManager.cList){
			Inventory inv = elem.getInventory();
			if(inv.getPair(this) != null){
				inv.getPair(this).getItem().name = name;
			}
		}
		this.name = name;
		
		Store.save();
		CharManager.save();
		Common.saveData(ITEM_FILE, masterList);
	}
	
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	
	public boolean isUsable() {
		return isUsable;
	}
	public void setUsable(boolean isUsable) {
		this.isUsable = isUsable;
	}
	
	public String getUseMsg() {
		return useMsg;
	}
	public void setUseMsg(String useMsg) {
		this.useMsg = useMsg;
	}
	
	public Item use(){
		if(prizes == null) return null;
		java.util.Random random = new java.util.Random();
		return prizes[random.nextInt(prizes.length)];
	}
	public void setPrizes(Item [] prizes){
		this.prizes = prizes;
	}
	
	public Catagory getCategory(){
		return catagory;
	}
	
	public static void load(){
		Common.readData(ITEM_FILE, masterList);
	}
}// end
