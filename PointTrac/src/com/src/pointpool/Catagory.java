package com.src.pointpool;

import java.io.Serializable;

public enum Catagory implements Serializable{
	FOOD,
	LUXURY,
	EQUIPMENT,
	MEDICINE,
	HERB,
	SPORTS,
	WEAPON_CLOSE_RANGE,
	WEAPON_LONG_RANGE,
	CLOTHING_CASUAL,
	CLOTHING_SEXY,
	CLOTHING_ARMOR,
	SCHOOL_SUPPLY,
	MISC,
	CHEST,
	BOOK,
	SPELL;
}
