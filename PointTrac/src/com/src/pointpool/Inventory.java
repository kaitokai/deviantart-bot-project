package com.src.pointpool;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public class Inventory implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -103054665552444653L;
	private ArrayList<ItemPair> inventory = new ArrayList<>();
	
	// c_tor
	public Inventory(){
		
	}// end 
	
	// routines
	/**
	 * POSTCONDITION: Adds the item into the inventory list if it wasn't
	 * there to begin with. 
	 * 
	 * @param item
	 * @param quantity
	 * @return <tt>true</tt> if the item exists and is not in the inventory<br>
	 * <tt>false</tt> otherwise.
	 */
	public boolean addItem(String item, int quantity){
		// grab the item to see if it exists in the database
		Item itm = Item.getItem(item);
		if(itm == null || quantity < 0) return false;
		return addItem(itm, quantity);
	}// end routine 

	public boolean addItem(Item item, int quantity){
		for(ItemPair elem : inventory){
			if(elem.getItem().equals(item)){
				elem.modifyCount(quantity);
				return true;
			}
		}
		ItemPair pair = getPair(item);
		if(pair == null){		
			inventory.add(new ItemPair(item, quantity));
			Collections.sort(inventory);
			return true;
		}
		pair.modifyCount(quantity);
		return true;
	}
	
	/**
	 * Routine does not handle for inventory quantity < quantity. In that
	 * condition, the item will be removed from the inventory.
	 * @param item
	 * @param quantity
	 * @return <tt>false</tt>, if the item does not exist within the inventory
	 * or if the quantity is negative. <br/>
	 * <tt>true</tt>, if the item exists.
	 * 
	 */
	public boolean takeItem(String item, int quantity){
		Item itm = Item.getItem(item);
		if(itm == null || quantity < 0) return false;
		
		ItemPair pair = getPair(itm);
		if(pair == null) return false;
		if(pair.getQuantity() <= quantity){
			removeFromInventory(pair);
			return true;
		}
		pair.modifyCount(-quantity);
		return true;
	}// end routine 
	
	public boolean exchangeItem(Inventory inv, String itemName, int quantity){
		ItemPair itemPair = this.getPair(itemName);
		// this handles the case where the item doesn't exist and isn't in
		// the inventory
		if(itemPair == null){
			System.out.println("Issue");
			return false;
		}
		
		// does the inventory have enough of the item in stock to fulfill the 
		// requested quantity amount?
		if(itemPair.getQuantity() < quantity) return false;
		
		// remove the amount requested from our inventory's ItemPair
		takeItem(itemName, quantity);
		
		// next we add the item to the inventory
		
		return inv.addItem(itemName, quantity);
	}// end routine exhangeItem
	
	public boolean removeFromInventory(ItemPair pair){
		if(!inventory.remove(pair)) return false;
		
		return true;
	}// end routine removeFromInventory
	
	public boolean removeFromInventory(String name){
		return removeFromInventory(getPair(name));
	}//
	
	/**
	 * 
	 * @param item
	 * @return
	 */
	public int getItemCount(String item){
		ItemPair itm = getPair(item);
		if(itm == null) return -1;
		return itm.getQuantity();
	}// end routine getItemCount
	
	/**
	 * @return the total number of <tt>Item</tt> objects in the 
	 * <tt>Inventory</tt>.
	 */
	public int getCount(){
		return inventory.size();
	}// end routine getCount
	
	public String toInvString(){
		return toInvString(1);
	}// end routine getItems
	
	public String toInvString(double salePrcnt){
		StringBuilder sb = new StringBuilder();
		
		for(ItemPair elm : inventory){
			Item item = elm.getItem();
			sb.append("<b>Item:</b> " + item.getName() + 
					" <b>Cost:</b> " + (int)Math.ceil((item.getCost() * salePrcnt)) + 
					" <b>Quantity:</b> " + elm.getQuantity() + 
					" <b>Item Type:</b> " + elm.getItem().getCategory().toString() +
					" <b>Consumable?</b> " + elm.getItem().isUsable() + "\n" +
					"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i><b>Description:</b> " + 
					item.getDesc() + "</i>\n");
		}
		if(sb.length() == 0){
			sb.append("<i>There's nothing here!</i>");
		}
		return sb.toString();
	}

	/**
	 * Grabs the item pairing to accommodate for the item name and the quantity
	 * on hand
	 * 
	 * @param name
	 * @return <tt>ItemPair</tt> if found otherwise <tt>null</tt>
	 */
	private ItemPair getPair(String name){
		return getPair(Item.getItem(name));
	}
	protected ItemPair getPair(Item item){
		if(item == null) return null;
		for(ItemPair elem : inventory){
			if(elem.getItem().equals(item)){
				return elem;
			}
		}
		return null;
	}// end
}// end class

class ItemPair implements Serializable, Comparable<ItemPair>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6967493273116255779L;
	// fields
	private final Item item;
	private int quantity;
	
	// c_tor
	public ItemPair(Item item, int quantity){
		this.item = item;
		this.quantity = quantity;
	}// end routine ItemPair
	
	// ROUTINES
	/**
	 * <p>Adds quantity to current count, use negative number to subtract count
	 * 
	 * @param quantity
	 */
	public void modifyCount(int quantity){
		this.quantity += quantity;
	}// end routine addItem

	// accessor and mutators
	public Item getItem() {
		return item;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	protected static ItemPair getPair(Item item, ArrayList<ItemPair> lst){
		for(ItemPair elem : lst){
			if(elem.getItem().equals(item)){
				return elem;
			}
		}
		return null;
	}

	@Override
	public int compareTo(ItemPair o) {
		return this.getItem().compareTo(o.getItem());
	}
}
