package com.src.pointpool;

import java.io.Serializable;

public class Brand implements Serializable{
	private String brandName;
	private String modelName;
	private int versionNumber;
	private int totalWorth;
}
