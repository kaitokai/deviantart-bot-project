package com.src.source;

import java.io.*;
import java.util.*;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.src.pointpool.*;
import com.src.pointtrack.*;

public class Common {
	public static final boolean DEBUGGING = false;
	private static ObjectOutputStream oos;
	private static ObjectInputStream ois;
	public static final String PATH = "data/pointtrack/";
	
	public synchronized static void saveData(String fileName, List collection){
		try{
			oos = new ObjectOutputStream(new FileOutputStream(PATH + fileName));
			for(Object elm : collection){
				oos.writeObject(elm);
			}
			oos.close();
		}catch(IOException e){
			
		}
		oos = null;
	}
	
	public synchronized static void saveData(String fileName, int number){
		try{		
			PrintWriter pw = new PrintWriter(new FileOutputStream(PATH + fileName));
			pw.print(number);
			pw.flush();
			pw.close();
		}catch(IOException e){
			
		}
	}
	
	public synchronized static void readData(String fileName, List collection){
		ois = null;
		long start = System.nanoTime();
		System.out.println("Reading: " + fileName + ":");
		try{
			File file = new File(PATH + fileName);
			if(!file.exists()){
				new File(file.getCanonicalFile().getParent()).mkdirs();
				file.createNewFile();
				return;
			}
			ois = new ObjectInputStream(new FileInputStream(file));
			while(true){
				Object o = ois.readObject();
				collection.add(o);
			}
		}catch(IOException e){
			// finished reading file
		}catch(ClassNotFoundException e){
			e.printStackTrace();
		}
		System.out.println("Finished reading " + collection.size() + 
				" records in: " + ((System.nanoTime() - start) * 10E-9) + " seconds.");
	}// end routine 
	
	public synchronized static int readData(String fileName){
		int temp = 0;
		try{
			System.out.println("Reading file:" + fileName);
			Scanner input = new Scanner(new FileInputStream(PATH + fileName));
			temp = input.nextInt();
			input.close();
			System.out.println("Read completed");
		}catch(IOException e){
			System.out.println("File not read.");
		}
		return temp;
	}// end routine
	
	public synchronized static void xmlSaveData(String fileName, Object[] lst){
		JAXBContext context;
		Marshaller m;
		try {
			context = JAXBContext.newInstance(lst[0].getClass());
			m = context.createMarshaller();
			FileOutputStream fos = new FileOutputStream(PATH + fileName);
			for(Object o : lst){
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				m.marshal(o, System.out);
			}
		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (IOException ex){
			ex.printStackTrace();
		}
	}
	/**
	 * 
	 */
	public static void backup(){
		
	}// end routine
	
	public static void loadAll(){
		new PointTrack().load();
		CharManager.load();
		PointPoolSystem.load();
		Item.load();
	}
	
	/**
	 * <p>FOR TESTING PURPOSE: DO NOT USE IN PRODUCTION
	 * <p>POSTCONDITION: All database nodes are empty
	 */
	public static void ejectCoreData(){
		
	}
}
