package com.src.source;

public class SyntaxParser {
	public static void main(String[] args){
		String info = "\"Archer Walker\" buys 10 \"arrows\" from \"blackwire arms\"";
		System.out.println(parse(info.split(" ")));
		System.out.println(parse(info.split(" ")).split("\\|")[1]);
	}
	public static final String divider = "\\|:|";
	
	public static String parse(String... args){
		String info = "";
		for(String elem : args){
			info += elem + " ";
		}
		
		java.util.Scanner input = new java.util.Scanner(info);
		StringBuilder modded = new StringBuilder();
		
		boolean inQuotes = false;
		while(input.hasNext()){
			String next = input.next();
			if(next.startsWith("\"")){
				inQuotes = true;
				next = next.substring(1);
			}
			if(next.endsWith("\"")){
				next = next.substring(0, next.length() - 1);
				inQuotes = false;
			}
			modded.append(next + ((inQuotes)?" ":"|"));
		}
		input.close();
		
		return modded.toString();
	}
}
