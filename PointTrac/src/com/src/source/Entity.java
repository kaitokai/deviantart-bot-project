package com.src.source;

import java.io.Serializable;

import com.src.pointpool.Inventory;

public abstract class Entity implements Serializable{
	protected String name;
	protected Inventory inventory = new Inventory();
	protected int value;
	
	public String getName(){ return name; }
	public void setName(String name){
		this.name = name;
	}
	public Inventory getInventory() {
		return inventory;
	}
	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}
	
	public abstract boolean addPoints(Entity entity, int points, String reason);
	public abstract boolean givePointsTo(int points, Entity entity, String reason);

	/**
	 * <p> Abstract item trading routine that allows for two entities to swap
	 * items in inventory.<br>
	 * PRECONDITION: <br>
	 * POSTCONDITION: Item counts are changed appropriately<br>
	 * @param itemName
	 * @param quantity
	 * @param entity
	 * @return <tt>true</tt>, if the item exists within the inventory and has the
	 * 		   proper item count to trade.<br>
	 * 		   <tt>false</tt>, if the item is in insufficient quantity, if the
	 *         item is not in the entity inventory, or item does not exist.
	 */
	public abstract boolean tradeItem(String itemName, int quantity, Entity entity);

	/**
	 * Gets the current amount of points usable by this entity
	 * @return points from a source
	 */
	public abstract int getPoints();
}
