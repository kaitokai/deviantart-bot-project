package com.src.missionpool;

import java.util.ArrayList;

import com.src.pointpool.Item;
import com.src.pointtrack.FCharacter;
import com.src.pointtrack.Rank;

public class MissionPool {
	
	public MissionPool(){
		
	}
	
	class Mission{
		private String name;

		private MissionTag tag;
		private int number;
		
		private Rank minRank; // minimum rank in order to perform mission
		
		private int currPts; // current number of points 
		private int maxiPts; // max number of points for reasonable success
		private Item[] requiredItems; // not all missions need items
		
		private ArrayList<FCharacter> participantsList;
		
		private Item[] rewards;
	}
}

enum MissionTag{
	HUNTING,
	REPAIR,
	INVESTIGATE;
}

enum MissionOutcome{
	DISASTER,
	DIRE,
	BAD,
	OKAY,
	GOOD,
	GREAT;
}