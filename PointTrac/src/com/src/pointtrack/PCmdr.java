package com.src.pointtrack;

import com.src.pointpool.Catagory;
import com.src.pointpool.Item;
import com.src.pointpool.PointPoolSystem;
import com.src.pointpool.Store;
import com.src.source.SyntaxParser;

/**
 * 
 * @author KLE
 * @version 1.0.5
 * 
 * Handles all input from IndigoBot for better command updates and features
 */
public class PCmdr {
	public static PointTrack pt = new PointTrack();
	private PCmdr(){
	}
	
	/* Error Strings */
	public static final String 
	ERROR_1 = "An error has occurred, please check formatting and try again",
	ERROR_2 = "User may not be in tracker, check username and try again";
	
	public static String moderatorPointTracker(String cmdName, String caller, String... args){
		String commandsList = 
		"<b>User Management:</b>\n " +
		"<sub>[!PTKR ADDUSER [usr]]" +
		"[!PTKR RMUSER [usr]]" +
		"[!PTKR RENAME [usr] [newName]]"+
		"</sub>\n<b>Character Management:</b>\n" +
		"<sub>[!PTKR ADDCHAR [usr] [charName] [age] [rank] [major]|[minor]]" +
		"[!PTKR RMCHAR [usr] [charName]]" +
		"[!PTKR CHGRANK [new rank] [usr] [char name]]" +
		"[!PTKR CHARACTERLIST]" +
		"</sub>\n<b>Point Management:</b>\n" +
		"<sub>[!PTKR ADDPTS [num] [usr]|[reason]]" +
		"[!PTKR RMPTS [usr] [num] |[reason]]" +
		"[!PTKR DELPTS [usr] [num]|[reason]]</sub>\n"+
		"<b>General Information:</b>\n" +
		"<sub>[!PTKR CIRC]" +
		"[!PTKR LOGS [num]|[type]|[moderator|username]]" +
		"[!PTKR VERSION]</sub>\n"+
		"<b>Command Information:</b>\n " +
		"<sub>[!PTKR HELP]</sub>";
		StringBuilder sb = new StringBuilder();
		String cmd = (args.length == 0)? "help" : args[0].toLowerCase();
		int paramSize = args.length - 1; // remove cmd
		switch(cmd){
		case "help":
			sb.append(commandsList);
			break;
		case "version":
			sb.append(PointTrack.printVersionInfo());
			break;
		case "adduser":
			if(paramSize == 2){ // condition for adding user with points
				try{
					int pts = Integer.valueOf(args[2]); // location of pts
					if(pt.addUser(args[1], pts)){
						sb.append("User: " + args[1] + " has been added, with points.");
					}
					else{
						sb.append(args[1] + " may be in tracker already.");
					}
				}catch(NumberFormatException ex){
					sb.append(ERROR_1);
				}
			}
			else if(paramSize == 1){ // other condition
				if(pt.addUser(args[1])){
					sb.append("User: " + args[1] + " has been added to the tracker.");
				}
				else{
					sb.append(args[1] + " may be in tracker already.");
				}
			}
			else{
				sb.append(cmdName + " ADDUSER [username] [pts]");
			}
			break;
		case "rmuser": // REMOVE USER
			if(paramSize == 1){
				if(pt.removeUser(args[1])){
					sb.append("User: " + args[1] + " has been removed from tracker.");
				}
				else{
					sb.append(ERROR_2);
				}
			}
			else{
				sb.append(cmdName + " RMUSER [username]");
			}
			break;
		case "rename": // COMMAND RENAME
			if(paramSize == 2){ // FMT: rename [username] [newUserName]
				if(pt.renameUser(args[1], args[2])){
					sb.append("<b>" + args[1] + "</b> has been changed to <b>" + 
					args[2] + "</b>");
					CharManager.updatedUserInfo(args[1], args[2]);
				}
				else{
					sb.append("An error has occured make sure username " +
							"exists and/or new name is not taken");
				}
			}
			else{
				sb.append(cmdName + " RENAME [username] [newname]");
			}
			break;
		case "addchar":
			if(5 <= paramSize){
				try{
					int age = Integer.valueOf(args[3]);
					Rank rank = Rank.valueOf(args[4].toUpperCase());
					Meister major = Meister.valueOf(args[5].toUpperCase());
					Meister minor = (paramSize == 5)?Meister.NONE
							:Meister.valueOf(args[6].toUpperCase());
					if(CharManager.addCharacter(args[1], args[2], age, rank, major, minor)){
						sb.append(args[1] + "'s character " + args[2] + " has been added to the tracker");
					}
					else{
						sb.append(args[2] + " has already been added under " + args[1]);
					}
				}catch(NumberFormatException ex){
					sb.append(ERROR_1 + ": check number value for error");
				}catch(IllegalArgumentException ex){
					sb.append(ERROR_1 + ": Possibly with RANK or MEISTER");
				}
			}
			else{
				sb.append(ERROR_1);
			}
			break;
		case "rmchar":
			if(paramSize == 2){
				CharManager.removeCharacter(args[1], args[2]);
				sb.append("Character " + args[2] + " removed from tracker");
			}
			break;
		case "chgrank":
			// chgrank [new rank] [usr] [char name]
			// 0	   1		  2		3
			if(paramSize == 3){
				try{
					Rank rank = Rank.valueOf(args[1]);
					FCharacter chara = CharManager.getCharacter(args[2], args[3]);
					if(chara != null){
						chara.setRank(rank);
						sb.append("Rank change successful");
					}
					else{
						sb.append("Either user and/or character are not registered.");
					}
				}catch(IllegalArgumentException ex){
					sb.append(ERROR_1 + ": Issue with the rank");
				}
			}
			else{
				sb.append("SYNTAX: chgrank [new rank] [usr] [char name]");
			}
			break;
		case "charlist":
		case "characterlist":
		case "roster":
		case "viewchars":
		case "lschars":
			sb.append(CharManager.printCharacters());
			break;
		case "addpts":
			pointModification(POINTS.ADD, cmdName, caller, sb, args);
			break;
		case "rmpts":
			pointModification(POINTS.REMOVE, cmdName, caller, sb, args);
			break;
		case "delpts":
			pointModification(POINTS.DELETE, cmdName, caller, sb, args);
			break;
		case "circ":
			sb.append(pt.getPointCirculation());
			break;
		case "logs":
			//FMT: 
			if(paramSize < 1 || 3 < paramSize){
				sb.append(cmdName + " LOGS [number]|[type]|[mod]");
			}
			else{
				try{
					int num = Integer.valueOf(args[1]);
					int rtype = PointTrack.VIEW_ALL_TRANS;
					if(2 <= paramSize){
						String strtype = args[2].toLowerCase();
						switch(strtype){
						case "exp":
						case "xp":
						case "utils":
						case "pts":
						case "pnts":
						case "points":
							rtype = PointTrack.VIEW_POINT_TRANS;
							break;
						case "dnt":
						case "donate":
						case "donuts":
						case "donations":
							rtype = PointTrack.VIEW_DONATE_TRANS;
							break;
						case "purchases":
						case "purch":
							rtype = PointTrack.VIEW_PURCHASE_TRANS;
							break;
						case "market":
							rtype = PointTrack.VIEW_SALE_TRANS;
							break;
						default:
							break;
						}
					}
					String usr = (paramSize < 3)?PointTrack.VIEW_ALL_USERS:
						args[3];
					sb.append(pt.printNTrans(num, rtype, usr, true));
				}catch(NumberFormatException ex){
					sb.append(ERROR_1);
				}
			}
			break;
		default:
			sb.append(commandsList);
			break;
		}
		return sb.toString();
	}
	
	private static void pointModification(POINTS points, String cmdName, String caller, 
			StringBuilder sb, String... args){
		// FMT: Xpts [number] [username] | [reason]
		int paramSize = args.length - 1;

		if(paramSize < 2){
			sb.append(cmdName + points.cmd + " [number] [username] | [reason]");
			return; // exit point here
		}
		try{
			StringBuilder reason = new StringBuilder("");
			int number = Integer.valueOf(args[1]); // number
			String usr = args[2];
			if (3 < paramSize){
				// 
				for(int i = 3; i < args.length; i++){
					reason.append(args[i] + " "); 
				}
			}
			switch(points){
			case ADD:
				if(pt.addPoints(caller, usr, number, reason.toString())){
					sb.append(points.val + " " + number + " points to " + usr);
				}
				else{
					sb.append(ERROR_1);
				}
				break;
			case REMOVE:
				if(pt.removePoints(caller, usr, number, reason.toString())){
					sb.append(points.val + " " + number + " points from " + usr);
				}
				else{
					sb.append(ERROR_1);
				}
				break;
			case DELETE:
				if(pt.deletePoints(caller, usr, number, reason.toString())){
					sb.append(points.val + " " + number + " points from " + usr);
				}
				else{
					sb.append(ERROR_1);
				}
				break;
			case DONATE:
				if(pt.donatePoints(caller, usr, number)){
					sb.append(points.val + " " + number + " to " + usr);
				}
				else{
					sb.append(ERROR_1);
				}
				break;
			default:
				break;
			}
		}catch(NumberFormatException ex){
			sb.append(ERROR_1);
		}
	}// end helper routine
	
	public static String userPointTracker(String cmdName, String caller, String... args){
		StringBuilder sb = new StringBuilder();
		String commandsList = "[!PTS VIEW [usr|all|none]][!PTS DONATE [usr] [num]]";
		String cmd = args[0].toLowerCase();
		int paramSize = args.length - 1;
		switch(cmd){
		case "view":
			if(paramSize == 1){
				String usr = args[1]; // grab the username
				if(usr.equalsIgnoreCase("all")){
					sb.append(pt.daTableList());
				}
				else{
					sb.append(pt.getUserInfor(usr));
				}
			}
			else{
				sb.append(pt.getUserInfor(caller));
			}
			break;
		case "donate":
			if(paramSize < 2){
				sb.append(cmdName + " DONATE [USERNAME][NUM]");
			}
			else{
				pointModification(POINTS.DONATE, cmdName, caller, sb, args);
			}
			break;
		case "mylog":
			sb.append(pt.printNTrans(-1, PointTrack.VIEW_ALL_TRANS, true, caller));
			break;
		default:
			sb.append(commandsList);
			break;
		}		
		return sb.toString();
	}
	
	public static String shopManager(String cmdName, String caller, String... args){
		StringBuilder sb = new StringBuilder();
		String cmdSyn = "<b>!" + cmdName.toUpperCase() + "</b>";
		
		String commandList = "<b>Shop Management:</b><sub>\n" +
				"[" + cmdSyn + " mkshop [shop][owner][operator][description]\n" +
				"[" + cmdSyn + " rmshop [shop]]\n" +
				"[" + cmdSyn + " addshopinv [quantity][item][shop]|[special inv? true | false]]\n" +
				"[" + cmdSyn + " rmfromshop [item][shop][personal? true or false]]\n" +
				"[" + cmdSyn + " choperator [shop][operator]\n" +
				"[" + cmdSyn + " lsshop]\n" + 
				"[" + cmdSyn + " restocktime]\n" +
				"</sub><b>Item Management:</b><sub>\n" +
				"[" + cmdSyn + " mkitem [name][desc][cost][useable? true or false][use msg][category]]\n" +
				"[" + cmdSyn + " mktchest [chest name] [desc] [cost] [item 1] [item 2] ... [item n]]\n" +
				"[" + cmdSyn + " rmitem [item]]\n" +
				"[" + cmdSyn + " renameitem [item name] to [new name]]\n" +
				"[" + cmdSyn + " addtoinv [quantity][item][user][character]]\n" +
				"[" + cmdSyn + " rmfrominv [item][user][char name]]\n" +
				"[" + cmdSyn + " lsitems]\n" +
				"</sub><b>TCA General Funds:</b><sub>\n" +
				"[" + cmdSyn + " genfunds]\n" +
				"NOTES: special inventory is inventory that is not restocked.";

		args = SyntaxParser.parse(args).split("\\|"); // proper parsing
		
		String cmd = (args.length == 0)?"help":args[0].toLowerCase();
		int paramSize = args.length - 1;
		switch(cmd){
		case "mkshop":
			// create a shop, so long as the shop doesn't exist already
			// mkshop [shop name] [owner] [operator] [description]
			// 0	  1			  2		  3			 4
			if(paramSize != 4){
				sb.append("SYNTAX: mkshop [shop name] [owner] [operator] [description]");
				break;
			}
			if(Store.createInstance(args[1], args[2], args[3], args[4], 0) == null){
				sb.append("Store " + args[1] + " already exists.");
			}
			else{
				sb.append(args[1] + " has been created.");
			}
			break;
		case "delshop":
		case "rmshop":
			// rmshop [shop name]
			if(paramSize != 1){
				sb.append("SYNTAX: rmshop [shop]");
			}

			if(Store.removeStore(args[1]) != null){
				sb.append("Store " + args[1] + " has been removed");
			}
			else{
				sb.append("Store is currently not in the system.");
			}
			break;
		case "addshopinv":
			// addshopinv [quantity] [item] [shop] [personal? true | false]
			// 0		  1		 	 2			3	   4
			if(paramSize == 3 || paramSize == 4){
				Store store = Store.getStore(args[3]);
				Item item = Item.getItem(args[2]);
				if(store == null){
					sb.append("Shop does not exist.");
				}
				else if(item == null){
					sb.append("Item does not exist");
				}
				else{
					try{
						int count = Integer.parseInt(args[1]);
						boolean isPersonal = (paramSize == 4)?Boolean.parseBoolean(args[4]):false;
						if(isPersonal){
							if(store.addSpecialItem(item.getName(), count))
								sb.append("Added " + item.getName() + " to special inventory.");
							else
								sb.append("Item already exists in shop inventory.");
						}
						else{
							if(store.addRegularStock(item.getName(), count))
								sb.append("Added " + item.getName() + " to regular inventory.");
							else
								sb.append("Item already exists in regular inventory.");
						}
					}catch(NumberFormatException ex){
						sb.append("Check the number field");
					}
				}
			}
			else{
				sb.append("SYNTAX: addshopinv [quantity] [item] [shop] [special inv? true | false]");
			}
			break;
		case "rmfromshop":
			// rmfromshop [item] [shop] [personal? true or false]
			// 0		  1		 2		3
			if(paramSize == 3){
				Store store = Store.getStore(args[2]);
				Item item = Item.getItem(args[1]);
				if(store == null){
					sb.append("Cannot find shop: Check the spelling or verify store existence.");
				}
				else if(item == null){
					sb.append("This item does not exist, check spelling and try again.");
				}
				else if(Boolean.parseBoolean(args[3])){
					if(store.getInventory().removeFromInventory(args[1]))
						sb.append("Removed item from store special inventory.");
					else
						sb.append("Item is not in store special inventory.");
				}
				else{
					if(store.removeFromStock(item))
						sb.append("Removed item from store regular inventory.");
					else
						sb.append("Item is not in store regular inventory.");
				}
			}
			else{
				sb.append("SYNTAX: rmfromshop [item] [shop] [personal? true or false]");
			}
			break;
		case "choperator":
		case "chngoptr":
			// choperator [shop] [operator]
			// 0		  1		 2
			if(paramSize == 2){
				Store store = Store.getStore(args[1]);
				if(store == null){
					sb.append("Cannot find shop: Check the spelling or verify store existence.");
				}
				sb.append("Operator for " + store.getName() + " has been switched from " + 
						store.getOperator() + " to ");
				store.setOperator(args[2]);
				sb.append(store.getOperator());
			}
			else{
				sb.append("SYNTAX: choperator [shop] [operator]");
			}
			break;
		case "lsshops":
			sb.append(Store.listStores());
			break;
		case "mkitem":
			// !shopmngr mkitem [name] [desc] [cost] [useable] [use msg] [category]
			//  X	 	 0		1	   2	  3		 4 		   5		 6
			if(paramSize == 6){
				try{
					int cost = Integer.valueOf(args[3]);
					boolean isUsable = Boolean.parseBoolean(args[4]);
					Catagory cate = Catagory.valueOf(args[6].toUpperCase());
					if(Item.createItem(args[1], args[2], cost, isUsable, args[5], cate)){
						sb.append("Item: " + args[1] + " has been created.");
					}
					else{
						sb.append("UH-OH! This item already exists!");
					}
				}catch(NumberFormatException ex){
					sb.append("ERROR: Check your cost field\n");
				}catch(IllegalArgumentException ex){
					sb.append("ERROR: Check your category field\n");
				}
			}
			else{
				sb.append("Check your syntax: \n");
				sb.append("!shopmngr mkitem [name: enquote] " +
						"[desc: enquote] " +
						"[cost: number] " +
						"[useable: true or false] " +
						"[use msg: enquote] " +
						"[category]\n");
				sb.append("<sub>Catagories:");
				for(Catagory i : Catagory.values())
					sb.append(i + ", ");
			}
			break;
		case "mktchest":
			// mktchest [chest name] [desc] [cost] [item 1] [occur1] [item 2] [occur 2] ... [item n]
			// 0		1			 2		3	   4		5		 ...
			try{
				if(5 <= paramSize){
					int cost = Integer.valueOf(args[3]);
					Item item = Item.getItem(args[1]);
					if(item == null){
						java.util.ArrayList<Item> prizes = new java.util.ArrayList<>();
						
						int i; 
						for(i = 4; i < paramSize + 1; i += 2){
							item = Item.getItem(args[i]);
							int occurance = Integer.valueOf(args[i + 1]);

							if(args[1].equalsIgnoreCase(args[i])){
								Item.createItem(args[1], args[2], cost, new Item[]{});
								sb.append("Infinite box made!\n");
								item = Item.getItem(args[1]);
							}
							
							if(item == null){
								sb.append("Item " + item + " does not exist.\n");
								break;
							}
							else{
								for(int j = 0; j < occurance; j++){
									prizes.add(item);
								}
							}
						}
						if(i != paramSize + 1){
							System.out.println(i + 1);
							System.out.println(paramSize);
							sb.append("Check item list.");
						}
						else{
							Item[] prize = new Item[prizes.size()];
							prizes.toArray(prize);
							if(!Item.createItem(args[1], args[2], cost, prize)){
								item = Item.getItem(args[1]);
								item.setPrizes(prize);
							}
							sb.append("Item has been birthed into existence.");
						}
					}
					else{
						sb.append("Item already exists.");
					}
				}
				else{
					sb.append("mktchest [chest name] [desc] [cost] [item 1] [item 2] ... [item n]");
				}
			}catch(NumberFormatException ex){
				sb.append("Check number field");
			}
			break;
		case "delitem":
		case "rmitem":
			// rmitem [item name]
			if(paramSize == 1 && Item.deleteItem(args[1])){
				sb.append("Item has been removed from existence.");
			}
			else{
				sb.append("Item may not exist");
			}
			break;
		case "rnitem":
		case "renameitem":
			// renameitem [item name] to [new name]
			// 0		  1			  2	 3
			if(paramSize == 3){
				Item check = Item.getItem(args[3]);
				if(check == null){
					Item item = Item.getItem(args[1]);
					sb.append(item.getName() + " ");
					item.setName(args[3]);
					sb.append("has been renamed to " + item.getName());
				}
				else{
					sb.append(args[3] + ": Item name already taken.");
				}
			}
			else{
				sb.append("SYNTAX: renameitem [item name] to [new name]");
			}
			break;
		case "lsitems":
			// lsitems [category]
			// 0	   1
			if(paramSize == 1){
				try{
					sb.append(Item.itemListByCatagory(Catagory.valueOf(args[1].toUpperCase())));
				}catch(IllegalArgumentException ex){
					sb.append("Category doesn't exist.");
				}
			}
			else{
				sb.append(Item.itemList());
			}
			break;
		case "addtoinv":
			// addtoinv [quantity] [item] [user] [char name]
			// 0		1	   	   2	  3		 4
			if(paramSize == 4){
				try{
					int quantity = Integer.parseInt(args[1]);
					
					Item item = Item.getItem(args[2]);
					if(item == null){
						sb.append("Check item field.");
						break;
					}
					
					FCharacter chara = CharManager.getCharacter(args[3], args[4]);
					if(chara == null){
						sb.append("Character or user not registered.");
						break;
					}
					
					chara.getInventory().addItem(item.getName(), quantity);
					sb.append("Added " + quantity + ((quantity != 1)?" units":" unit")
							+ " of " + item.getName() + " to " + chara.getName() + "'s inventory.");
					CharManager.save();// save all info
				}catch(NumberFormatException ex){
					sb.append("Check number argument");
				}
			}
			else{
				sb.append("SYNTAX: addtoinv [quantity] [item] [user] [char name]");
			}
			break;
		case "rmfrominv":
			// rmfrominv [item] [user] [char name]
			// 0	 	 1		2	   3		
			if(paramSize == 3){
				Item item = Item.getItem(args[1]);
				if(item == null){
					sb.append("Please check item field.");
					break;
				}
				
				FCharacter chara = CharManager.getCharacter(args[2], args[3]);
				if(chara == null){
					sb.append("Character or user not registered");
					break;
				}
				
				if(chara.getInventory().removeFromInventory(item.getName()))
					sb.append("Item has been removed from character inventory.");
				else
					sb.append("Error: item cannot be removed from inventory");
			}
			else{
				sb.append("SYNTAX: rmfrominv [item] [user] [char name]");
			}
			break;
		case "restock":
			break;
		case "restocktime":
			sb.append(PointPoolSystem.getRestockSchedule());
			break;
		case "genfund":
			// !shopmngr genfund
			sb.append("TCA General Funds: " + PointPoolSystem.getGenFunds() + " points.");
			break;
		case "shopinfo":
			// shopinfo [shop name]
			break;
		case "help":
		default:
			sb.append(commandList);
			break;
		}
		return sb.toString();
	}// end routine shopManager
	
	private static Store location; // TODO:
	public static String charTracker(String cmdName, String caller, String... args){
		// !char
		String cmdCap = "<b>!" + cmdName.toUpperCase() + "</b>";
		String commandList = "<b>Character Info:</b><sub>\n" +
				"[" + cmdCap + " stats [char name]]: character stats\n" +
				"[" + cmdCap + " inv [char name]]: character inventory\n" + 
				"</sub><b>Character Interactions:</b><sub>\n" +
				"[" + cmdCap + " [char name] buys [quantity][item] from [store]]:purchasing items\n" +
				"[" + cmdCap + " [char name] uses [item]]:using an item if consumable\n" +
				"[" + cmdCap + " [char name] breaks [quantity][item]]:destroying an item out of inventory\n" + 
				"[" + cmdCap + " [char name] gives [quantity][item] to [username][character]]\n" +
				"[" + cmdCap + " [char name] sells [quantity][item]]:Sell items for half base cost\n" +
				"</sub><b>Shop Information:</b><sub>\n" +
				"[" + cmdCap + " listshops]: lists all shops\n" +
				"[" + cmdCap + " viewshop [store]|[category]]: view a shops inventory\n";
		
		StringBuilder sb = new StringBuilder();
		args = SyntaxParser.parse(args).split("\\|");
		sb.append(charHelper(commandList, caller, (args.length == 0)?"help":args[0].toLowerCase(), args));
		if(sb.length() == 0 && 1 < args.length){
			System.out.println("run");
			String temp = args[0];
			args[0] = args[1]; 
			args[1] = temp;
			sb.append(charHelper(commandList, caller, args[0], args));
		}
		if(sb.length() == 0){
			sb.append(commandList);
		}
		return sb.toString();
	}

	private static String charHelper(String cmdList, String caller, String cmd, String... args){
		StringBuilder sb = new StringBuilder();
		FCharacter chara = null;
		int paramSize = args.length - 1;
		switch(cmd){
		case "stats":
			// stats [char name]
			// 0	 1
			chara = CharManager.getCharacter(caller, args[1]);
			sb.append("<b>Name:</b> " + chara.getName() + "\n");
			sb.append("<b>Age:</b> " + chara.getAge() + "\n");
			sb.append("<b>Rank:</b> " + chara.getRank() + "\n");
			sb.append("<b>Major:</b> " + chara.getMeisterMajor()  + "\n");
			sb.append("<b>Minor:</b> " + chara.getMeisterMinor()  + "\n");
			break;
		case "inv":
			// inv 'char name'
			if(paramSize == 1){
				chara = CharManager.getCharacter(caller, args[1]);
				if(chara == null){
					sb.append("Character may not be in tracker");
				}
				else{
					// get the inventory
					sb.append("<b><u>" + chara.getName() + "'s Inventory:</u></b>\n");
					sb.append(chara.getInventory().toInvString() + "\n");
				}
			}
			else{
				sb.append("Too many arguments");
			}
			break;
		case "purchases":
		case "buy":
		case "buys":
			// [char name] buys [quantity] [item] from [shop]
			// turns into
			// buys [char name] [quantity] [item] from [shop]
			//  0	1	  		2		   3	  4	   5
			if(paramSize == 5){
				FCharacter byr = CharManager.getCharacter(caller, args[1]);
				if(byr == null){
					sb.append("Character may not be in tracker");
					break;
				}
				Store slr = Store.getStore(args[5]);
				if(slr == null){
					sb.append("Store may not exist.");
					break;
				}
				if(!Item.hasItem(args[3])){
					sb.append("Item may not exist");
					break;
				}
				try{
					Integer quan = Integer.parseInt(args[2]);
					if(PointPoolSystem.buy(byr, slr, args[3], quan)){
						sb.append(byr.getName() + " has purchased " + quan + " " + args[3]);
					}
					else{
						sb.append("Store may not have the requested quantity or" +
								" you may not have enough points. (Check with !pts view)");
					}
				}catch(NumberFormatException ex){
					sb.append("Check the quantity value.\n");
				}
			}
			else{
				sb.append("SYNTAX: [char name] buys [quantity] [item] from [shop]");
			}
			break;
		case "sells":
			// sells [char name] [item]
			// 0	 1			 2
			// sells [char name] [quantity] [item]
			// 0	 1			 2			3
			if(2 <= paramSize){
				if((Item.hasItem(args[2]) || Item.hasItem(args[3])) &&
						CharManager.hasCharacter(caller, args[1])){
					try{
						int quantity = (paramSize == 3)?Integer.valueOf(args[2]):1;
						Item item = (paramSize == 2)?Item.getItem(args[2]):Item.getItem(args[3]);
						chara = CharManager.getCharacter(caller, args[1]);
						int count = chara.getInventory().getItemCount(item.getName());
						if(count == -1){
							sb.append("This character doesn't have this item.");
						}
						else if(count < quantity){
							sb.append("Quantity on hand too low, check stock and try again.");
						}
						else{
							int total = (item.getCost()/2) * quantity;
							if(pt.sellerPoints(caller, total, "+" + total + ", " +chara.getName() +
									" sold " + quantity + " x " + item.getName())){
								sb.append(chara.getName() + " successfully sold " + quantity + 
										" unit(s) of " + item.getName() + " for " + total);
								chara.getInventory().takeItem(item.getName(), quantity);
								CharManager.save();
							}
							else{
								sb.append("Oops! Something went wrong.");
							}
						}
					}catch(NumberFormatException ex){
						sb.append("Check number field.");
					}
				}
			}
			else{
				sb.append("[char name] sells [quantity] [item]");
			}
			break;
		case "trade":
			break;
		case "hwstat":
			break;
		case "breaks":
			// breaks [char name] [item]
			// 0	  1			  2		
			// breaks [char name] [quantity] [item]
			// 0	  1			  2			 3
			if(2 <= paramSize){
				try{
					int quantity = (paramSize==3)?Integer.valueOf(args[2]):1;
					String itemName = (paramSize == 2)?args[2]:args[3];

					if(!Item.hasItem(itemName)){
						sb.append("Item does not exist");
						break;
					}
					if(!CharManager.hasCharacter(caller, args[1])){
						sb.append("You do not have this character registered.");
						break;
					}
					
					chara = CharManager.getCharacter(caller, args[1]);
					if(chara.getInventory().takeItem(itemName, quantity)){
						sb.append(chara.getName() + " has utterly obliterated " + itemName
								+ " x " + quantity);
						CharManager.save();
					}
					else{
						sb.append("Check the item quantity in your inventory.");
					}
				}catch(NumberFormatException ex){
					sb.append("Check number field.");
				}
			}
			else{
				sb.append("Check your syntax\n");
				sb.append("!char [char name] breaks [quantity][item]");
			}
			break;
		case "builds":
		case "crafts":
		case "makes":
			break;
		case "drinks":
		case "eats":
		case "uses":
			// uses  [char name] [item]
			// 0	 1			 2
			if(paramSize == 2){
				Item item = Item.getItem(args[2]);
				chara = CharManager.getCharacter(caller, args[1]);
				if(item != null && !item.isUsable()){
					sb.append("Item is not usable");
				}
				else if(chara == null){
					sb.append("Character does not exist");
				}
				else if(!chara.getInventory().takeItem(args[2], 1)){
					sb.append("Does not have this item.");
				}
				else{
					Item prize = item.use();
					if(prize != null){
						chara.getInventory().addItem(prize, 1);
						sb.append(chara.getName() + " opens the " + item.getName() +
								" and finds...\n" + prize.getName() + "!");
					}
					else{
						sb.append(chara.getName() + " " + item.getUseMsg());
					}
					CharManager.save();
				}
			}
			break;
		case "gifts":
		case "gives":
			// caine gives quant item to con archer
			// gives [your char] [quantity] [item] to [user] [char name]
			// 0	 1			 2			3	   4  5		 6
			if(paramSize == 6){
				try{
					chara = CharManager.getCharacter(caller, args[1]);
					if(chara == null){
						sb.append("Character not registered under your name.");
						break;
					}
					
					int quantity = Integer.valueOf(args[2]);
					
					Item item = Item.getItem(args[3]);
					if(item == null){
						sb.append("Item does not exist.");
						break;
					}
					
					FCharacter receiver = CharManager.getCharacter(args[5], args[6]);
					if(receiver == null){
						sb.append("Destination character doesn't exist");
						break;
					}
					
					if(PointPoolSystem.trade(chara, receiver, quantity, item.getName())){						
						sb.append(chara.getName() + " gifted " + quantity +
								" " + item.getName() + " to " + receiver.getName());					
						CharManager.save();
					}
					else{
						sb.append("Insure that you have the item and the proper " +
								"quantity in your character inventory");
					}
				}catch(NumberFormatException ex){
					sb.append("Check number field.");
				}
			}
			else{
				sb.append("SYNTAX: [your char] gives [quantity][item] to [user][char]");
			}
			break;
		case "shops":
		case "viewshops":
		case "listshops":
			sb.append(Store.listStores());
			break;
		case "shop":
		case "viewshop":
			// viewshop [store] | [category]
			// 0		1		  2
			if(1 <= paramSize){
				Store store = Store.getStore(args[1]);
				if(store == null){
					sb.append("Cannot find shop: Check the spelling or verify store existence.");
				}
				else if(paramSize == 2){
					
				}
				else{
					sb.append(store.printInventory());
				}
			}
			else{
				sb.append("viewshop [store] | [category]");
			}
			break;
		case "help":
			sb.append(cmdList);
			break;
		default:
			break;
		}
		return sb.toString();
	}
}// end class PCmdr
