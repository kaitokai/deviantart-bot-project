/**
 * @author Kait
 * @version 1.0.5
 * @date 5.14.2013
 * 
 */
package com.src.pointtrack;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.io.*;

import com.src.source.Common;

import static java.util.Calendar.*;

public class PointTrack{
	/* FILE LOCATIONS */
	private final String TRANS_FILE = "trans.txt"; // local filename for point
													// tracking
	private final static String USER_FILE = "users.dat"; //
	// private final String XML_FILE = "sitelog";
	// private final String LOGGING_FILE = ""; //

	/* FIELDS */
	private static ArrayList<User> users = new ArrayList<>();
	private static ArrayList<String> transaction = new ArrayList<>();
	private static int currentPoints = 0;
	private static int totalPoints = 0;
	
	private static final String VERSION = "1.5.11"; 
	
	// STATIC INTEGERS
	public static final int VIEW_ALL_TRANS = 0;
	public static final int VIEW_POINT_TRANS = 1;
	public static final int VIEW_DONATE_TRANS = 2;
	public static final int VIEW_PURCHASE_TRANS = 3;
	public static final int VIEW_SALE_TRANS = 4;
	
	// STATIC STRINGS
	public static final String VIEW_ALL_USERS = "";

	/* Constructor */
	// private PointTrack(){}
	public PointTrack(){
	}
	
	/* Version query */
	public static String getVersion(){
		return VERSION;
	}
	public static String printVersionInfo(){
		return "PointTracker Version: " + VERSION + 
				"\n<sub>:devkaitokai: with collaboration "+
				"from :devbesieged-exorcists:";
	}
	
	/* Routines */
	/**
	 * <p>
	 * PRECONDITION:
	 * <p>
	 * POSTCONDITION: New user added to database
	 * 
	 * @return <tt>false</tt>, if user is already in db 
	 * 		   <tt>true</tt>, successful entry
	 */
	public boolean addUser(User usr) {
		int loc = Collections.binarySearch(users, usr);
		if(0 <= loc) return false;
		
		currentPoints += usr.getPoints();
		totalPoints += usr.getTotalPoints();
		users.add(usr);
		Collections.sort(users); // sort all user information to be written

		saveUserInfo();
		return true;
	}// end routine addUser

	public boolean addUser(String usr, int points) {
		if (points < 0)
			return false;
		return addUser(new User(usr, points));
	}// end routine addUser
	
	public boolean addUser(String usr, int currentPts, int totalPts){
		if(currentPts < 0) return false;
		if(totalPts < 0) return false;
		return addUser(new User(usr, currentPts, totalPts));
	}

	public boolean addUser(String usr) {
		return addUser(usr, 0);
	}
	
	/**
	 * 
	 * <p>POSTCONDITION: User is removed from list. Both current and total
	 * points are destroyed out of the system.
	 * 
	 * @param usr
	 * @return <tt>false</tt>, user is not in db <br>
	 * 		   <tt>true</tt>, successful removal
	 */
	public boolean removeUser(User usr) {		
		int loc = Collections.binarySearch(users, usr);
		if(loc < 0) return false;
		
		User elem = users.get(loc);
		currentPoints -= elem.getPoints();
		totalPoints -= elem.getTotalPoints();
		users.remove(loc);
		
		saveUserInfo();
		return true;
	}// end routine removeUser

	public boolean removeUser(String usr) {
		return removeUser(new User(usr));
	}// end routine removeUser
	
	/**
	 * <p>Simple routine to rename a user account to another
	 * 
	 * @param usr
	 * @param name
	 * @return <tt>true</tt>, if username is changed<br>
	 * 		   <tt>false</tt> otherwise
	 */
	public boolean renameUser(String usr, String name){
		User e;
		if(((e = getUser(usr)) != null) && (getUser(name) == null)){
			e.setName(name);
			saveUserInfo();
			return true;
		}
		else{
			return false;
		}
	}// end routine renameUser
	
	public boolean hasUser(String usr) {
		return users.contains(new User(usr));
	}// end routine hasUser

	/**
	 * <p>POSTCONDITION: The structure of the List is left unchanged
	 * 
	 * @param usr, string name of user
	 * @return <tt>null</tt>, if user is not in db <tt>User</tt>, if user is
	 *         located
	 */
	protected User getUser(String usr) {
		for (User elem : users) {
			if ((new User(usr)).equals(elem)) {
				return elem;
			}
		}
		return null;
	}// end routine getUser

	/**
	 * 
	 * @param usr
	 * @return returns the string format of a user [User=usr, points=pts] if
	 *         user is not in db, returns the string: usr + " doesn't exits"
	 */
	public String getUserInfor(String usr) {
		User user = getUser(usr);
		if (user == null)
			return usr + " doesn't exist";

		return getUser(usr).toString();
	}// end routine getUserInfo

	/**
	 * 
	 * @return returns a string representation of all users in database
	 */
	public String displayAllUsers() {
		StringBuilder sb = new StringBuilder();
		for (User elems : users) {
			sb.append(elems.tableString() + '\n');
		}
		return sb.toString();
	}// end routine displayAllUsers

	public String daTableList() {
		StringBuilder sb = new StringBuilder();
		String line = "---------------------------------------------\n";
		sb.append("<code>" + line);
		sb.append("#"
				+ DAFmts.daSpcFmtr("Current Points", 27, 1)
				+ DAFmts.daSpcFmtr("#", 17, 1)
				+ "\n");
		sb.append(line);
		sb.append("|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + 
				DAFmts.daSpcFmtr("User Name", 5, 0) + 
				DAFmts.daSpcFmtr("|", 7, 1) +
				DAFmts.daSpcFmtr(" C. pts", 3, 0) + "&nbsp;&nbsp;&nbsp;|" + 
				DAFmts.daSpcFmtr(" T. pts", 3, 0) + "&nbsp;&nbsp;&nbsp;|\n");
		sb.append(line);
		for (User elems : users) {
			sb.append("|" + elems.daTableString() + '\n');
		}
		sb.append(line);
		sb.append("|Total" 
				+ DAFmts.daSpcFmtr("|", 22-5, 1)
				+ DAFmts.daSpcFmtr(String.valueOf(currentPoints), 10, 1) + "|"
				+ DAFmts.daSpcFmtr(String.valueOf(totalPoints), 10, 1) + "|"
				+ "\n");
		sb.append(line);
		return sb.toString();
	}

	/**
	 * 
	 * @param usr
	 *            <tt>String</tt> representation of the username to act on
	 * @param pts
	 *            integer to evaluate on
	 * @return <tt>false</tt>, if user does not exist -or- action could not be
	 *         completed
	 */
	public boolean removePoints(String usr, int pts) {
		User temp = getUser(usr);
		if (temp == null)
			return false;

		if(temp.removePoints(pts)){
			currentPoints -= pts;
			saveUserInfo();
			return true;
		}
		return false;
	}// end routine removePoints

	/**
	 * <p>
	 * PRECONDITION: User must exist in db
	 * <p>
	 * POSTCONDITION: Must call outputTransaction, points must be removed from
	 * the usr
	 * <p>
	 * MSG FMT: USERNAME + removed + PTS [ REASON ]
	 * 
	 * @param usr
	 *            user to deduct points from
	 * @param pts
	 *            integer to be evaluated on
	 * @param reason
	 *            <tt>String</tt> that explains reason for routine call
	 * @return <tt>false</tt>, if user does not exist -or- action could not be
	 *         completed
	 */
	public boolean removePoints(String caller, String usr, int pts,
			String reason) {
		if (!removePoints(usr, pts))
			return false;
		logTransaction(VIEW_POINT_TRANS, caller, " Removed " + pts + " points from "
				+ usr + " [" + reason + "]");
		return true;
	}// end removePoints
	public boolean purchasedPoints(String usr, int pts, String reason){
		if(!removePoints(usr, pts)) return false;
		logTransaction(VIEW_PURCHASE_TRANS, usr, reason);
		return true;
	}
	public boolean sellerPoints(String usr, int pts, String reason){
		if(!addPoints(usr, pts)) return false;
		logTransaction(VIEW_SALE_TRANS, usr, reason);
		return true;
	}

	/**
	 * Deletes points from existence in the database for correcting point
	 * errors. Uses the removePoints method with additional subtraction call
	 * user total and global total.
	 * <p>
	 * PRECONDITION: User must exist in db, load() must be called if using saved
	 * data
	 * <p>
	 * POSTCONDITION: Points are removed from both the current and total points
	 * from the user.
	 * 
	 * <p>
	 * MSG FMT: USERNAME + removed + PTS [ REASON ]
	 * 
	 * @param caller
	 * @param usr
	 * @param pts
	 * @param reason
	 * @return <tt>true</tt>, if removal is successful
	 */
	public boolean deletePoints(String caller, String usr, int pts,
			String reason) {
		if (!removePoints(usr, pts))
			return false;
		// assumption that the points are okay after passing the typical point
		// removal from above.
		getUser(usr).removeTotal(pts);
		totalPoints -= pts;
		logTransaction(VIEW_POINT_TRANS, caller, " Deleted " + pts + " points from "
				+ usr + " [ " + reason + " ]");
		saveUserInfo();
		return true;
	}// end routine deletePoints

	public boolean addPoints(String usr, int pts) {
		User temp = getUser(usr);
		if (temp == null)
			return false;
		
		if(!temp.addPoints(pts))
			return false;
		saveUserInfo();
		currentPoints += pts;
		totalPoints += pts;
		return true;
	}// end routine addPoints

	/**
	 * <p>
	 * This version of the routine makes a call to the Transaction Log directly.
	 * Made for the chatroom version.
	 * 
	 * <p>
	 * PRECONDITION: User must exist in the db
	 * <p>
	 * POSTCONDITION: Must call outputTransaction, points are added to the user
	 * 
	 * <p>
	 * MSG FMT: USERNAME + added + PTS, REASON
	 * 
	 * @param usr
	 *            user to deduct points from
	 * @param pts
	 * @param reason
	 * @return <tt>true</tt>, if point addition is successful
	 */
	public boolean addPoints(String caller, String usr, int pts, String reason) {
		if (!addPoints(usr, pts))
			return false;
		logTransaction(VIEW_POINT_TRANS, caller, "Added " + pts + " points to " + usr
				+ "[ " + reason + " ]");
		return true;
	}// end routine addPoints

	/**
	 * <p>Shift points from one user to another (or the same user)
	 * 
	 * <p>PRECONDITIONS: Both users exist in the db
	 * <p>POSTCONDITIONS: Appropriate points removed from caller and added to usr
	 * account
	 * 
	 * <p>MSG FORMAT: CALLER has donated PTS points to USR
	 * 
	 * @param caller
	 *            the user to donate points
	 * @param usr
	 *            recipient of points
	 * @param pts
	 *            positive integer to transfer to usr
	 * @return <tt>true</tt>, if transaction is completed <tt>false</tt>, if pts
	 *         is a negative value or either user is not present in the db or if
	 *         points are not sufficient in caller acct
	 */
	public boolean donatePoints(String caller, String usr, int pts) {
		if (pts < 10)
			return false;

		User donor = getUser(caller);
		User recipient = getUser(usr);
		// exception UserDoesNotExistException?
		if (donor == null || recipient == null)
			return false;
		if (!donor.removePoints(pts))
			return false;

		recipient.addPoints(pts);
		logTransaction(VIEW_DONATE_TRANS, caller, "Donated " + pts + " points to " + usr);
		saveUserInfo();
		return true;
	}// end routine donatePoints

	public String getPointCirculation() {
		return "<b>Points in Circulation:</b><sub>\n" 
				+ "Current Points: " + currentPoints + "\n"
				+ "Total Points: " + totalPoints;
	}// end routine

	/**
	 * <p> Routine appends most recent transaction, mandatory by all routines
	 * that alter points. 
	 * 
	 * @param callType integer representation of the call
	 * @param caller 
	 * @param fmt defined by the function calling this routine
	 */
	private final int RLOC = 0, DATELOC = 1, MSGLOC = 2, USRLOC = 0;
	private void logTransaction(int callType, String caller, String fmt) {
		Calendar now = Calendar.getInstance();
		String date = (now.get(MONTH) + 1) + "/" + now.get(DATE) + "/"
				+ now.get(YEAR);
		String prnt = callType + "|[" + date + "]|" + caller + ": " + fmt;
		PrintWriter ps = null;
		try {
			ps = new PrintWriter(new FileWriter(Common.PATH + TRANS_FILE, true));
			ps.append(prnt + "\n");
			ps.flush();
			ps.close();
		} catch (IOException ex) {
		}
		transaction.add(prnt);
	}// end routine outputTransaction

	private void loadTransactions() {
		RandomAccessFile raf;
		try {
			raf = new RandomAccessFile(Common.PATH + TRANS_FILE, "r");
			String tmp = null;
			while ((tmp = raf.readLine()) != null) {
				transaction.add(tmp);
			}
			System.out.printf(transaction.size() + " transactions loaded...");
			raf.close();
		} catch (FileNotFoundException ex) {
		} catch (IOException ex) {
		}
	}// end routine readTransaction

	/**
	 * <p>Prints transactions as a string
	 * @param num
	 *            positive numbered integer for the number of lines passed
	 *            desired
	 * @return string with the number of lines provided by the user if the
	 *         transactions < num, then the entire log will be printed
	 */
	public String printNTrans(int num, boolean showDate) {
		return printNTrans(num, VIEW_ALL_TRANS, VIEW_ALL_USERS, null, showDate);
	}// end routine printNTransactions

	public String printNTrans(int num, int rtype, boolean showDate) {
		return printNTrans(num, rtype, VIEW_ALL_USERS, null, showDate);
	}// end routine
	public String printNTrans(int num, int rtype, String usr, boolean showDate) {
		return printNTrans(num, rtype, usr, null, showDate);
	}
	public String printNTrans(int num, int rtype, boolean showDate, String user){
		return printNTrans(num, rtype, VIEW_ALL_USERS, user, showDate);
	}
	public String printNTrans(int num, int rtype, String usr, String user, 
			boolean showDate) {
		StringBuilder sb = new StringBuilder();
		// Print Format: RTYPE|DATE|CALLER:MSG
		for(int i = transaction.size() - 1, j = 0; (num == -1 || j < num) 
				&& (0 <= i); i--){
			String[] inter = transaction.get(i).split("\\|"); // place holder
			String modCmp = inter[2].split("\\:")[USRLOC];
			if(Integer.valueOf(inter[RLOC]) == rtype || rtype == VIEW_ALL_TRANS){
				if(usr.equalsIgnoreCase(modCmp) || usr.equals(VIEW_ALL_USERS)){
					if(user != null && !inter[MSGLOC].contains(user)) 
						continue;
					if(showDate){
						sb.append(inter[DATELOC] + " ");
					}
					sb.append(inter[MSGLOC] + "\n");
					j++; // we have found one of these fuckers
				}
			}
		}
		
		if (sb.length() == 0) {
			sb.append("No transactions available");
		}
		return sb.toString();
	}// end routine printNTrans

	/**
	 * Routine should be called when altering any User fields
	 */
	private void saveUserInfo() {
		Common.saveData(USER_FILE, users);
	}// end routine saveUserInfo

	private void readUserInfo() {
		Common.readData(USER_FILE, users);
		for(User elm : users){
			currentPoints += elm.getPoints();
			totalPoints += elm.getTotalPoints();
		}
	}// end routine readUserInfo

	/**
	 * <p>
	 * This command must always be run, will add to constructor for better
	 * control in future versions.
	 */
	public void load() {
		System.out.println("Loading PointTrack records...");
		long totalTime = System.nanoTime();
		readUserInfo();
		loadTransactions();
		totalTime = System.nanoTime() - totalTime;
		System.out.println("Complete for a total of: " + (totalTime * 10E-9));
	}// end routine load
	
	public User[] getUsers(){
		User[] a = new User[users.size()];
		return users.toArray(a);
	}
}// end class
