package com.src.pointtrack;

public enum LogType {
	ALL(0, null),
	ADD_POINTS(1, "Added"),
	REMOVE_POINTS(2, "Removed"),
	DONATION(3, "Donated"),
	PURCHASE(4, "Purchased"),
	SALE(5, "Sale");
	
	LogType(int loc, String strRep){
		this.loc = loc;
		this.strRep = strRep;
	}
	
	private int loc;
	private String strRep;
}
