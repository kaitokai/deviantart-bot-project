package com.src.pointtrack;

import com.src.pointpool.Inventory;
import com.src.source.Common;
import com.src.source.Entity;

public class FCharacter extends Entity implements Comparable<FCharacter>{
	private static final long serialVersionUID = 1L;

	private User owner;
	private int age;
	private int numTestsTaken = 0;
	private int numTestsPassed = 0;
	private Rank rank;
	private Meister meisterMajor;
	private Meister meisterMinor;
	private int homeworksCompleted;
	
	protected FCharacter(String name){
		this.name = name;
	}
	protected FCharacter(String name, String owner, int age){
		this.name = name;
		this.owner = PCmdr.pt.getUser(owner);
		this.age = age;
	}
	protected FCharacter(String name, String owner, int age, Rank rank,
			Meister meisterMajor, Meister meisterMinor){
		this.name = name;
		this.owner = PCmdr.pt.getUser(owner);
		this.age = age;
		this.rank = rank;
		this.meisterMajor = meisterMajor;
		this.meisterMinor = meisterMinor;
	}
	
	public boolean isUserNull(){
		if(owner==null) return true;
		return false;
	}// end routine isUserNull
	public String getUserName(){
		return owner.getName();
	}
	public boolean updateOwner(String newOwner){
		User user = PCmdr.pt.getUser(newOwner);
		if(user == null) return false;
		this.owner = user;
		return true;
	}
	
	@Override
	public boolean givePointsTo(int points, Entity entity, String reason) {
		if(points <= owner.getPoints()){
			entity.addPoints(entity, points, reason);
			PCmdr.pt.purchasedPoints(owner.getName(), points, reason);
			return true;
		}
		return false;
	}

	@Override
	public boolean tradeItem(String item, int quantity, Entity entity) {
		return inventory.exchangeItem(entity.getInventory(), item, quantity);
	}

	@Override
	public int getPoints() {
		return owner.getPoints();
	}

	public void setAge(int age){
		this.age = age;
	}
	public int getAge(){
		return age;
	}
	
	public void setRank(Rank rank){
		this.rank = rank;
	}// end routine setRank
	public Rank getRank(){
		return rank;
	}// end routine getRank
	
	public void setMeisterMajor(Meister meister){
		this.meisterMajor = meister;
	}
	public Meister getMeisterMajor(){
		return meisterMajor;
	}
	
	public void setMeisterMinor(Meister meister){
		this.meisterMinor = meister;
	}
	public Meister getMeisterMinor(){
		return meisterMinor;
	}

	public void setHomeworkNum(int num){
		this.homeworksCompleted = num;
	}
	public void modifyHomeworkNum(int num){
		this.homeworksCompleted += num;
	}
	public int getHomeworkNum(){
		return homeworksCompleted;
	}
		
	@Override
	public boolean addPoints(Entity entity, int points, String reason) {
		PCmdr.pt.addPoints(entity.getName(), owner.getName(), points, 
				reason);
		return false;
	}// end routine addPoints

	@Override
	public int compareTo(FCharacter o) {
		return this.name.toLowerCase().compareTo(o.name.toLowerCase());
	}
}// end class Character

