package com.src.pointtrack;

import java.util.*;
import static java.util.Calendar.*;
import java.io.*;

public class Tester {
	public static void main(String... args) {
		PointTrack pt = new PointTrack();
		Scanner sc;
		try {
			sc = new Scanner(new File("raw.txt"));
			while(sc.hasNext()){
				String line = sc.nextLine();
				String part[] = line.split("\\\t");
//				for (int i = 0; i < part.length; i++) {
//					System.out.println(part[i]);
//				}
				int currPts = Integer.valueOf(part[6]);
				int totPts = Integer.valueOf(part[7]);
				System.out.println(currPts + " " + totPts);
				pt.addUser(part[0], currPts, totPts);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e){
			e.printStackTrace();
		}
		pt.load();
//		System.out.println(pt.getUserInfor("kamillesamansname"));
		System.out.println(pt.displayAllUsers());
	}
}
