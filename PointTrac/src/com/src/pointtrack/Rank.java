package com.src.pointtrack;

public enum Rank {
	CIVILIAN,
	PAGE,
	EXWIRE,
	LOWER_2ND,
	LOWER_1ST,
	MIDDLE_2ND,
	MIDDLE_1ST,
	UPPER_2ND,
	UPPER_1ST,
	CHANCELLOR,
	ARCKNIGHT,
	PALADIN;
}// end 
