package com.src.pointtrack;

import java.util.ArrayList;
import java.util.Collections;
import java.io.*;

import com.src.pointpool.Item;
import com.src.source.Common;
public class CharManager {
	private static final String CHAR_DATA = "characters.dat";
	// static list of all characters in the program
	public static ArrayList<FCharacter> cList = new ArrayList<>();
	
	private CharManager(){};
	
	// Routine to be called in the start of each session in order to load all
	// information stored during last session.
	public static void load(){
//		readCharacterInfo();
		Common.readData(CHAR_DATA, cList);
	}// end routine load
	
	// adds a character into the database
	public static boolean addCharacter(String userName, String characterName, int age){
		return addCharacter(userName, characterName, age, null, null, null);
	}// end routine addCharacter

	public static boolean addCharacter(String userName, String characterName,
			int age, Rank rank, Meister major, Meister minor ){
		int cmp = Collections.binarySearch(cList, new FCharacter(characterName));
		if(0 <= cmp){ // key is found, character already exists
			return false;
		}
		FCharacter test = new FCharacter(characterName, userName, age, rank, major, minor);
		System.out.println(test.getName());
		if(test.isUserNull()){
			return false;
		}
		cList.add(test);
		Collections.sort(cList);
//		saveCharacterInfo();
		Common.saveData(CHAR_DATA, cList);
		return true;		
	}
	
	public static boolean removeCharacter(String owner, String characterName){
		int cmp = Collections.binarySearch(cList, new FCharacter(characterName));
		if(cmp < 0) return false;
		
//		cList.remove(cmp);
//		saveCharacterInfo();
		cList.remove(sweep(owner, characterName, cmp));
		
		Common.saveData(CHAR_DATA, cList);
		return true;
	}// end routine removeCharacter
	
	// Should not be called--routine destroys all characters from the list and
	// writes over everything in the save file.
	public static void removeAll(){
		cList.clear();
//		saveCharacterInfo();
		Common.saveData(CHAR_DATA, cList);
	}// end routine removal all characters
	
	/**
	 * Checks to see if character exists within the internal ArrayList, if so
	 * set the lastSeekedCharacter internal private variable to the fchar and
	 * then return true.
	 * 
	 * <br/>
	 * POSTCONDITION: If character is within database, set lSeekedChar to fchar
	 * 
	 * @param characterName
	 * @return <tt>true</tt>, if the character is in the database<br/>
	 * 		   <tt>false</tt>, otherwise.
	 */
	private static FCharacter lSeekedChar;
	public static boolean hasCharacter(String owner, String characterName){
		int cmp = Collections.binarySearch(cList, new FCharacter(characterName));
		if(cmp < 0){
			return false;
		}
		lSeekedChar = sweep(owner, characterName, cmp);
		return true;
//		return (cmp < 0)? false : true;
	}// end routine hasCharacter
	
	private static FCharacter sweep(String owner, String cName, int loc){
		FCharacter temp = cList.get(loc);
		if(!temp.getUserName().equalsIgnoreCase(owner)){
			// left
			boolean seekLeft = true;
			while(0 <= loc && loc < cList.size()){
				if(seekLeft)
					temp = cList.get(--loc);
				else
					temp = cList.get(++loc);
				if(temp.getName().equalsIgnoreCase(cName)){
					if(temp.getUserName().equalsIgnoreCase(owner)){
						break;
					}
				}
				else if(!seekLeft){
					return null;
				}
				else{
					seekLeft = false;
				}
			}
		}
		return temp;
	}// end routine
	
	/**
	 * 
	 * @param characterName
	 * @return the character in request, returns <tt>null</tt> if character is
	 * not located in database.
	 */
	public static FCharacter getCharacter(String owner, String characterName){
		if(hasCharacter(owner, characterName)){
			return lSeekedChar;
		}
		return null;
	}// end routine retrieveChar
	
	/**
	 * Generally handles exchange of character between Users. Best used when
	 * a User's name is changed or when a User trades character off.
	 * 
	 * @param oldUser old user name
	 * @param newUser new user name
	 */
	public static void updatedUserInfo(String oldUser, String newUser){
		for(FCharacter elem : cList){
			if(elem.getUserName().equalsIgnoreCase(oldUser)){
				elem.updateOwner(newUser);
			}
		}
	}// end routine updatedUserInfo
	
	public static void removeItemFromInventory(Item item){
		for(FCharacter elem : cList){
			elem.getInventory().removeFromInventory(item.getName());
		}
		save();
	}
	
	public static void save(){
		Common.saveData(CHAR_DATA, cList);
	}// end routine save

	public static String printCharacters() {
		StringBuilder sb = new StringBuilder();
		sb.append("<b><code>" + DAFmts.daSpcFmtr("CHARACTER", 15, DAFmts.LEFT) +
				"USER</code></b><code>\n");
		for(FCharacter elem : cList){
			sb.append(DAFmts.daSpcFmtr(elem.getName(), 15, DAFmts.LEFT) + 
					elem.getUserName() + "\n");
		}
		return sb.toString();
	}// end routine
	// cmds:
	// !goto [shopname] [school]
	// !buy [[charactername] [item] [quantity]|[view]]
	// !trade [charactername] [item] [destination] [quantity]
	// !where?, displays where you're at
}
