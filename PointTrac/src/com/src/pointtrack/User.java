package com.src.pointtrack;
/**
 * 
 */
import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class User implements Serializable, Comparable<User>{
	private static final long serialVersionUID = -5414237725779783896L;
	private String name;
	private int points;
	private int totalPoints;
	
	/* Constructors */
	private User(){
		
	}
	public User(String name, int points){
		this.name = name;
		this.points = points;
		this.totalPoints = points;
	}
	public User(String name, int currentPts, int totalPts){
		this.name = name;
		this.points = currentPts;
		this.totalPoints = totalPts;
	}
	public User(String name){
		this(name, 0);
	}
	
	/* Accessors & Mutators */
	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name = name;
	}
	
	public int getPoints(){
		return points;
	}
	public int getTotalPoints(){
		return totalPoints;
	}

	/* Routines */
	/**
	 * Routine adds points to the user, use negative numbers to deduct
	 * POSTCONDITION: Points added to user
	 * @param points points to be added to this user
	 * @return <tt>true</tt>, if points are successfully added
	 */
	public boolean addPoints(int points){
		if(0 < points){
			this.points += points;
			this.totalPoints += points;
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @param points
	 * @return <tt>false</tt>, not enough points in account
	 * 		   <tt>true</tt>, successfully removed points from acct
	 */
	public boolean removePoints(int points){
		if(this.points < points || points < 0){
			return false;
		}
		this.points -= points;
		return true;
	}// end routine removePoints
	
	public boolean removeTotal(int value){
		if(this.totalPoints < value || value < 0){
			return false;
		}
		totalPoints -= value;
		return true;
	}// end routine removeFromTotal
	
	public void clearPoints(){
		points = 0;
		totalPoints = 0;
	}// end routine clearPoints
	
	@Override
	public boolean equals(Object obj){
		if(!(obj instanceof User)) return false;
		if(this == obj) return true;

		User temp = (User)obj;
		if(this.name.equalsIgnoreCase(temp.name)) return true;
		return false;
	}

	@Override
	public String toString(){
		return "[Name= :dev" + name +":, C. Points=" + points + ", T. Points=" + totalPoints + "]";
	}
	
	public String daString(){
		return "[Name= :dev" + name + ": C. Points=" + points + 
				", T. Points=" + totalPoints + "]";
	}//end routine daString()
	
	public String tableString(){
		return "";
	}
	
	public String daTableString(){
		StringBuilder sb = new StringBuilder();
		sb.append(DAFmts.daSpcFmtr(name, 21, 0) + "|");
		sb.append(DAFmts.daSpcFmtr(String.valueOf(points), 10, 1) + "|");
		sb.append(DAFmts.daSpcFmtr(String.valueOf(totalPoints), 10, 1) + "|");
//		sb.append(String.format("%-20s|%10d|%10d|", name, points, totalPoints));
		return sb.toString();
	}

	@Override
	public int compareTo(User o) {
		return this.name.toLowerCase().compareTo(o.name.toLowerCase());
	}
}// end class user
