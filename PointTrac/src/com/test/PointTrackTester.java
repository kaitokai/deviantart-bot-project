package com.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.src.pointtrack.*;

public class PointTrackTester {	
	protected PointTrack pt;
	@Before
	public void setUp() throws Exception {
		pt = new PointTrack();
		pt.addUser("Kaitokai");
		pt.addUser("Conartist18");
		pt.addUser("IKArtStudios", 100);
	}

	@After
	public void tearDown() throws Exception {
		pt = null;
	}

	@Test
	public void testAddUser() {
		pt = new PointTrack();
		assertTrue(pt.addUser("Conartist18"));
		assertFalse(pt.addUser("Conartist18"));
		assertFalse(pt.addUser("bacon", -10));
	}

	@Test
	public void testRemoveUser() {
		assertTrue(pt.removeUser("IKArtStudios"));
		assertFalse(pt.removeUser("art--wolf"));
	}

	@Test
	public void testRemovePoints() {
		assertTrue(pt.removePoints("IKArtStudios", 100));
		assertFalse(pt.removePoints("conartist18", 100));
	}

	@Test
	public void testAddPoints() {
		assertTrue(pt.addPoints("conartist18", 1433));
		assertFalse(pt.addPoints("Yomi", 1337));
//		assertTrue(pt.addPoints("conartist18", "Kaitokai", 32, "http://fav.me/d620ecd"));
	}

	@Test
	public void testLoad() {
		pt = null;
		pt = new PointTrack();
		pt.load();
		assertTrue(pt.hasUser("IKartStudios"));
		assertFalse(pt.hasUser("Ka"));
	}
	
	@Test
	public void testDonate(){
//		pt.addUser("BangVipot");
//		pt.donatePoints("ikartstudios", "BangVipot", 10);
//		assertFalse(pt.donatePoints("conartist18", "kaitokai", 10));
//		assertFalse(pt.donatePoints("yfan", "infer", 10));
//		pt.donatePoints("Bang Vipot", "BangVipot", 10);
//		assertFalse(pt.donatePoints("bang vipot", "conartist18", -10));
	}
		
	@Test
	public void testGeneral(){
		pt = new PointTrack();
		pt.load();
//		System.out.println(pt.printNTrans(1, false));
//		System.out.println();
//		System.out.println(pt.printNTrans(-1, true));
//		System.out.println();
		System.out.println(pt.printNTrans(5, PointTrack.DONATE, false));
//		System.out.println();
//		System.out.println(pt.printNTrans(-1, PointTrack.DONATE, true));
//		System.out.println();
//		System.out.println(pt.printNTrans(-1, PointTrack.POINTS, "conartist18", true));
//		System.out.println();
//		System.out.println(pt.printNTrans(1, true));
//		System.out.println();
//		System.out.println(pt.printNTrans(1, true));
//		
//		System.out.println(pt.getTotalPoints());
	}
	
	@Test
	public void testdaListFormat(){
//		System.out.println(pt.daTableList());
	}
	
	@Test
	public void testgetUserInfo(){
		pt.getUserInfor("tommy");
	}
}// end test class
