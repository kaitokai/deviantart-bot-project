Simply placed into repository to view old projects that have been worked on independently.

Project was meant to be used in conjunction with Indigo Bot by BaronOBeanDip of deviantArt and the deviantArt Messenger Network (dAMN) before an overhaul of the authentication system wound up disabling Java's networking API from connecting. Unsure if issues were fixed concerning the error, but I'm no longer interested in working on this project. 

Uses a Java backend database in main memory (if I had to do this project over again, I would have just used SQLite and saved myself time in place of building a glorified SCRUD application with my own tools) and a BeanShell script attached as a plugin for the IndigoBot as a bridge to my backend. Intent was to have a 'on-the-go' update system for the bot program rather than taking the bot down and restarting it.

Project/experiment was concluded late 2013.